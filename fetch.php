<?php
/**
 * @author vladimir@goos.dk
 *
 */

ini_set('memory_limit', '1024M');
ini_set('max_execution_time', 3600);

class RepositoryManager {

	protected $projectSlug = '';
	protected $command = '';
	protected $config = array();

	function __construct($config) {
		$this->config = $config;
	}

	public function processRepositories() {
		if(!is_dir($this->config['repositories'])) {
			mkdir($this->config['repositories']);
		}
		$slugs = self::getRepositoriesSlugs();
		foreach($slugs as $repositoryName) {
			if(self::repositoryExists($repositoryName)) {
				self::updateLocalRepository($repositoryName);
			} else {
				self::createLocalRepository($repositoryName);
			}
		}
		exit(0);
	}

	private function getRepositoriesSlugs() {
		$url = $this->config['hostname'] . "/api/1.0/user/";
		$url_repositories = $url . "repositories/dashboard";

		$ch = curl_init($url_repositories);
		$options = array(
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_USERPWD => $this->config['username'] . ":" . $this->config['password'],
				CURLOPT_HTTPHEADER => array('Content-type: application/json')
		);
		curl_setopt_array($ch, $options);
		$json_result =  curl_exec($ch);

		$result = json_decode($json_result);

		$slugs = array();
		foreach($result[1][1] as $key => $repository) {
			$slugs[] = $repository->slug;
		}

		return $slugs;
	}

	private function createLocalRepository($repositoryName = '') {
		$url = "https://" . $this->config['username'] . ":" . $this->config['password'] . "@bitbucket.org/" . $this->config['repositoryRoot'] . "/" . $repositoryName;
		$this->command = 'git clone ' . escapeshellarg($url) . ' ' . $this->config['repositories'] . $repositoryName;
		$this->run();
		$this->zipFolder($this->config['repositories'] . $repositoryName, $this->config['repositories'] . $repositoryName . ".zip");
	}

	private function updateLocalRepository($repositoryName = '') {
		$url = "https://" . $this->config['username'] . ":" . $this->config['password'] . "@bitbucket.org/" . $this->config['repositoryRoot'] . "/" . $repositoryName;
		$this->command = 'git pull origin master';
		$this->command = sprintf('cd %s && %s', escapeshellarg($this->config['repositories'] . $repositoryName), $this->command);
		$this->run();
		$this->zipFolder($this->config['repositories'] . $repositoryName, $this->config['repositories'] . $repositoryName . ".zip");
		$this->uploadToAWS($repositoryName);
	}


	private function repositoryExists($repositoryName = '') {
		if(!file_exists($this->config['repositories'] . $repositoryName.'/.git/HEAD')) {
			return false;
		}
		return true;
	}

	private function run() {

		print $this->command."\n"; // turn off if you do not need output

		ob_start();
		passthru($this->command);
		$output = ob_get_clean();

		print $output . "\n"; // turn off if you do not need output
	}

	private function uploadToAWS($repositoryName) {

		$s3 = new S3($this->config['awsAccessKey'], $this->config['awsSecretKey']);

		$fileName   = $this->config['repositories'] . $repositoryName . ".zip";

		if($this->config['keepDailyBackups'] == TRUE) {
	 		$uploadName = $this->config['repositories'] . $repositoryName . "_" . date('d') . ".zip";
		} else {
			$uploadName = $this->config['repositories'] . $repositoryName . ".zip";
		}

		$s3->putObject(S3::inputFile($fileName, false), $this->config['bucketName'], $uploadName, S3::ACL_PUBLIC_READ);
	}

	private function zipFolder($source, $destination) {

		if (!extension_loaded('zip') || !file_exists($source)) {
			return false;
		}

		$zip = new ZipArchive();
		if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
			return false;
		}

		$source = str_replace('\\', '/', realpath($source));

		if (is_dir($source) === true)
		{
			$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

			foreach ($files as $file)
			{
				$file = str_replace('\\', '/', $file);

				// Ignore "." and ".." folders
				// Patched for .git folders
				if( in_array(substr($file, strrpos($file, '/')+1), array(/*'.', */'..')) )
					continue;

				$file = realpath($file);

				if (is_dir($file) === true)
				{
					$zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
				}
				else if (is_file($file) === true)
				{
					$zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
				}
			}
		}
		else if (is_file($source) === true)
		{
			$zip->addFromString(basename($source), file_get_contents($source));
		}

		return $zip->close();
	}

}

echo "<pre>";
chdir('archive');

include('S3.php');
include('config.php');

$manager = new RepositoryManager($config);
$manager->processRepositories();

echo "</pre>";

?>